import Vue from 'vue'
import Vuex from 'vuex'
import api from '@/api/api.js'

Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
		userInfo: uni.getStorageSync('userInfo') || '',
	},
	getters: {
		userInfo: state => state.userInfo,
	},
	mutations: {
		set_userInfo(state, data) {
			state.userInfo = data
			uni.setStorageSync('userInfo', data)
		},
	},
	actions: {
		set_userInfo({
			commit,
			state
		}, data) {
			commit('set_userInfo', data)
		},
	},
})

export default store
