##引入

```
export default {
	components: {
		uploadFileImg
	},
}

<uploadFile-img :showTap="showTap" num="9" :images="images"></uploadFile-img>
```



##传入说明：

|字段| 类型 | 实例 | 说明|
| --- |:---:| ---:| ---:|
|images | array | ["/1.png","/2.png"] |存放图片数组|
|num | Number	| 1 |最多可上传数量默认为1|
|showTap | Boolean | true |图片是否上传中 false上传中，true上传完成|

##微信小程序通过setshowTap,setimages方法修改showTap和images
```
setshowTap(e) {
	this.showTap = e
},
setimages(e) {
	this.images = e
},
```