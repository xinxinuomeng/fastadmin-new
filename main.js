import App from './App'
import api from './api/api.js'
import store from './store'

// #ifndef VUE3
import Vue from 'vue'
Vue.prototype.$store = store
Vue.prototype.$api = api
Vue.config.productionTip = false

App.mpType = 'app'
const app = new Vue({
    ...App,
	store
})
app.$mount()
// #endif


// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif